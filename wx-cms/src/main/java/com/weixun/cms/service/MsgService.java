package com.weixun.cms.service;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import java.util.List;

public class MsgService {
    /**
     * 分页
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Page<Record> paginate(int pageNumber, int pageSize,String msg_content) {
        StringBuffer  sqlstr = new StringBuffer();
        sqlstr.append("from t_msg s  where 1=1 ");
        if (msg_content != null && !msg_content.equals("")) {
            sqlstr.append("and s.msg_content like '%" + msg_content + "%' ");
        }
        sqlstr.append(" order by s.msg_pk desc ");

        String select = "select *";

        return Db.paginate(pageNumber, pageSize, select, sqlstr.toString());
//        return Db.paginate(pageNumber, pageSize, "select *", "from t_msg order by msg_pk desc");
    }


    /**
     * 获取列表
     * @return
     */
    public List<Record> findList(String msg_pk)
    {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from t_msg where 1=1 ");
        if (msg_pk != null && !msg_pk.equals(""))
        {
            stringBuffer.append("and msg_pk = "+msg_pk);
        }
        List<Record> list = Db.use("datasource").find(stringBuffer.toString());
        return list;
    }

    /**
     * 删除，可以根据多数据源删除
     * @param ids
     * @return
     */
    public int deleteById(String ids) {
        String sql = "delete from t_msg where msg_pk in ("+ids+")";
//        Db.delete("sys_user","user_pk",ids);
        Integer result = Db.use("datasource").update(sql);
        return result;
    }
}
